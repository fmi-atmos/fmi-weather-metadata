export type ForecastParameterMetaData = {
  [key in ForecastParameters]: IForecastParameter;
};

export enum ForecastParameters {
  CeilingHeight = 'CeilingHeight',
  ConvectionSeverityIndex = 'ConvectionSeverityIndex',
  DewPoint = 'DewPoint',
  DewPointDifference = 'DewPointDifference',
  DriftSnowIndex = 'DriftSnowIndex',
  FeelsLike = 'FeelsLike',
  FreezingLevelHeight = 'FreezingLevelHeight',
  Friction = 'Friction',
  FrostSum = 'FrostSum',
  FrostProbability = 'FrostProbability',
  GroundTemperature = 'GroundTemperature',
  HighCloudCover = 'HighCloudCover',
  HourlyMaximumGust = 'HourlyMaximumGust',
  LowCloudCover = 'LowCloudCover',
  MiddleCloudCover = 'MiddleCloudCover',
  PoP = 'PoP',
  PotentialPrecipitationForm = 'PotentialPrecipitationForm',
  Precipitation1h = 'Precipitation1h',
  Precipitation3h = 'Precipitation3h',
  Precipitation6h = 'Precipitation6h',
  Precipitation12h = 'Precipitation12h',
  Precipitation24h = 'Precipitation24h',
  PrecipitationForm = 'PrecipitationForm',
  ProbabilityOfRailRule5 = 'ProbabilityOfRailRule5',
  DailyPrecipitationProbabilityOver1mm = 'DailyPrecipitationProbabilityOver1mm',
  DailyPrecipitationProbabilityOver10mm = 'DailyPrecipitationProbabilityOver10mm',
  DailyPrecipitationProbabilityOver20mm = 'DailyPrecipitationProbabilityOver20mm',
  DailyPrecipitationProbabilityOver30mm = 'DailyPrecipitationProbabilityOver30mm',
  DailyPrecipitationProbabilityOver50mm = 'DailyPrecipitationProbabilityOver50mm',
  DailyPrecipitationProbabilityOver70mm = 'DailyPrecipitationProbabilityOver70mm',
  DailyPrecipitationProbabilityOver100mm = 'DailyPrecipitationProbabilityOver100mm',
  Pressure = 'Pressure',
  ProbabilityThunderstorm = 'ProbabilityThunderstorm',
  ProbabilityOfThunderPOT30 = 'ProbabilityOfThunderPOT30',
  ProbabilityDryRoad = 'ProbabilityDryRoad',
  ProbabilityFrostyRoad = 'ProbabilityFrostyRoad',
  ProbabilityIcyRoad = 'ProbabilityIcyRoad',
  ProbabilityMoistRoad = 'ProbabilityMoistRoad',
  ProbabilityPartlyIcyRoad = 'ProbabilityPartlyIcyRoad',
  ProbabilitySnowyRoad = 'ProbabilitySnowyRoad',
  ProbabilityWetRoad = 'ProbabilityWetRoad',
  ProbabilityWetSnowyRoad = 'ProbabilityWetSnowyRoad',
  ProbabilityOfPrecFormDrizzle = 'ProbabilityOfPrecFormDrizzle',
  ProbabilityOfPrecFormFreezingDrizzle = 'ProbabilityOfPrecFormFreezingDrizzle',
  ProbabilityOfPrecFormFreezingRain = 'ProbabilityOfPrecFormFreezingRain',
  ProbabilityOfPrecFormRain = 'ProbabilityOfPrecFormRain',
  ProbabilityOfPrecFormSnow = 'ProbabilityOfPrecFormSnow',
  ProbabilityOfPrecFormSleet = 'ProbabilityOfPrecFormSleet',
  ProbabilityTRoadLimit1 = 'ProbabilityTRoadLimit1',
  ProbabilityTRoadLimit2 = 'ProbabilityTRoadLimit2',
  ProbabilityOfSeaLevelLimit1 = 'ProbabilityOfSeaLevelLimit1',
  ProbabilityOfSeaLevelLimit2 = 'ProbabilityOfSeaLevelLimit2',
  ProbabilityOfSeaLevelLimit3 = 'ProbabilityOfSeaLevelLimit3',
  ProbabilityOfSeaLevelLimit4 = 'ProbabilityOfSeaLevelLimit4',
  ProbabilityOfSeaLevelLimit1N2000 = 'ProbabilityOfSeaLevelLimit1N2000',
  ProbabilityOfSeaLevelLimit2N2000 = 'ProbabilityOfSeaLevelLimit2N2000',
  ProbabilityOfSeaLevelLimit3N2000 = 'ProbabilityOfSeaLevelLimit3N2000',
  ProbabilityOfSeaLevelLimit4N2000 = 'ProbabilityOfSeaLevelLimit4N2000',
  QNH = 'QNH',
  RadarPrecipitationAccumulation5min = 'RadarPrecipitationAccumulation5min',
  RelativeHumidity = 'RelativeHumidity',
  RoadFrostProbability = 'RoadFrostProbability',
  RoadCondition = 'RoadCondition',
  RoadConditionSeverity = 'RoadConditionSeverity',
  RoadIceCover = 'RoadIceCover',
  RoadFrostCover = 'RoadFrostCover',
  RoadSnowCover = 'RoadSnowCover',
  RoadTemperature = 'RoadTemperature',
  RoadTemperatureF0 = 'RoadTemperatureF0',
  RoadTemperatureF10 = 'RoadTemperatureF10',
  RoadTemperatureF25 = 'RoadTemperatureF25',
  RoadTemperatureF50 = 'RoadTemperatureF50',
  RoadTemperatureF75 = 'RoadTemperatureF75',
  RoadTemperatureF90 = 'RoadTemperatureF90',
  RoadTemperatureF100 = 'RoadTemperatureF100',
  RoadNotification = 'RoadNotification',
  RoadWarning = 'RoadWarning',
  SeaLevel = 'SeaLevel',
  SeaLevelF10 = 'SeaLevelF10',
  SeaLevelF50 = 'SeaLevelF50',
  SeaLevelF90 = 'SeaLevelF90',
  SeaLevelN2000 = 'SeaLevelN2000',
  SeaLevelF10N2000 = 'SeaLevelF10N2000',
  SeaLevelF50N2000 = 'SeaLevelF50N2000',
  SeaLevelF90N2000 = 'SeaLevelF90N2000',
  SeaSurfaceTemperature = 'TemperatureSea',
  SevereFrostProbability = 'SevereFrostProbability',
  SigWaveHeight = 'SigWaveHeight',
  SnowfallProbability1hOver03mm = 'SnowfallProbability1hOver03mm',
  SnowfallProbability1hOver1mm = 'SnowfallProbability1hOver1mm',
  SnowfallProbability1hOver2mm = 'SnowfallProbability1hOver2mm',
  SnowfallProbability1hOver4mm = 'SnowfallProbability1hOver4mm',
  SnowfallProbability1hOver6mm = 'SnowfallProbability1hOver6mm',
  SnowfallProbability1hOver10mm = 'SnowfallProbability1hOver10mm',
  SnowfallProbability3hOver10mm = 'SnowfallProbability3hOver10mm',
  SnowfallProbability6hOver1mm = 'SnowfallProbability6hOver1mm',
  SnowfallProbability6hOver5mm = 'SnowfallProbability6hOver5mm',
  SnowfallProbability6hOver10mm = 'SnowfallProbability6hOver10mm',
  SnowfallProbability6hOver20mm = 'SnowfallProbability6hOver20mm',
  SnowfallProbability24hOver1mm = 'SnowfallProbability24hOver1mm',
  SnowfallProbability24hOver5mm = 'SnowfallProbability24hOver5mm',
  SnowfallProbability24hOver10mm = 'SnowfallProbability24hOver10mm',
  SnowfallProbability24hOver15mm = 'SnowfallProbability24hOver15mm',
  SnowfallProbability24hOver30mm = 'SnowfallProbability24hOver30mm',
  SnowfallProbability24hOver50mm = 'SnowfallProbability24hOver50mm',
  SmartSymbol = 'SmartSymbol',
  SmartSymbolText = 'SmartSymbolText',
  SnowAccumulation = 'SnowAccumulation',
  Temperature = 'Temperature',
  TemperatureProbabilityLessThan0 = 'TemperatureProbabilityLessThan0',
  TemperatureProbabilityLessThan20 = 'TemperatureProbabilityLessThan20',
  TemperatureProbabilityLessThan25 = 'TemperatureProbabilityLessThan25',
  TemperatureProbabilityLessThan30 = 'TemperatureProbabilityLessThan30',
  TemperatureProbabilityLessThan35 = 'TemperatureProbabilityLessThan35',
  TemperatureProbabilityMoreThan25 = 'TemperatureProbabilityMoreThan25',
  TemperatureProbabilityMoreThan27 = 'TemperatureProbabilityMoreThan27',
  TemperatureProbabilityMoreThan30 = 'TemperatureProbabilityMoreThan30',
  TotalCloudCover = 'TotalCloudCover',
  TotalWireSnowLoadChange = 'TotalWireSnowLoadChange',
  TotalWireSnowLoad = 'TotalWireSnowLoad',
  TotalCrownSnowLoadChange = 'TotalCrownSnowLoadChange',
  TotalCrownSnowLoad = 'TotalCrownSnowLoad',
  Visibility = 'Visibility',
  WaterStorage = 'WaterStorage',
  WindDirection = 'WindDirection',
  WindGust = 'WindGust',
  WindSpeedMS = 'WindSpeedMS',
  MeanWindSpeedProbabilityMoreThan11 = 'MeanWindSpeedProbabilityMoreThan11',
  MeanWindSpeedProbabilityMoreThan14 = 'MeanWindSpeedProbabilityMoreThan14',
  MeanWindSpeedProbabilityMoreThan17 = 'MeanWindSpeedProbabilityMoreThan17',
  MeanWindSpeedProbabilityMoreThan21 = 'MeanWindSpeedProbabilityMoreThan21',
  MeanWindSpeedProbabilityMoreThan25 = 'MeanWindSpeedProbabilityMoreThan25',
  WindGustProbabilityMoreThan15 = 'WindGustProbabilityMoreThan15',
  WindGustProbabilityMoreThan20 = 'WindGustProbabilityMoreThan20',
  WindGustProbabilityMoreThan25 = 'WindGustProbabilityMoreThan25',
  WindGustProbabilityMoreThan30 = 'WindGustProbabilityMoreThan30',
  WindGustProbabilityMoreThan35 = 'WindGustProbabilityMoreThan35',
}

export type IForecastParameter = {
  name: LocalizedString;
  type: ParameterType;
  unit?: string;
  requestParameters?: RequestParameters[];
} & (
  | {
      type: 'number';
      max?: number;
      min?: number;
    }
  | {
      type: 'mapped';
      values: IParameterOption[];
    }
  | {
      type: 'text';
      value?: string;
    }
);

export type LocalizedString = {
  [key in ParameterLocale]: string;
};

export enum ParameterLocale {
  fi = 'fi',
  en = 'en',
}

export type ParameterType = 'number' | 'mapped' | 'text';

export interface IParameterOption {
  name: LocalizedString;
  value: number;
}

export interface RequestParameters {
  parameter: string;
  producers: string[];
}

import { ForecastParameterMetaData } from './types';

export const forecastParameters: ForecastParameterMetaData = {
  CeilingHeight: {
    name: {
      fi: 'Pilvikorkeus',
      en: 'Ceiling',
    },
    unit: 'm',
    type: 'number',
    requestParameters: [
      { parameter: 'CEIL-2-M', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  ConvectionSeverityIndex: {
    name: {
      fi: 'Rajuilmojen vahinkopotentiaali',
      en: 'Convection Severity Index',
    },
    type: 'number',
    min: 0,
    requestParameters: [
      {
        parameter: 'CSI-N',
        producers: ['ECGMTA', 'MEPSMTA'],
      },
    ],
  },
  DewPoint: {
    name: {
      fi: 'Kastepistelämpötila',
      en: 'Dew point temperature',
    },
    unit: '°C',
    type: 'number',
  },
  DewPointDifference: {
    name: {
      fi: 'Kastepiste-ero',
      en: 'Dew point difference',
    },
    unit: '°C',
    type: 'number',
    requestParameters: [
      {
        parameter: 'dewpointdeficit',
        producers: [
          'roadmodel_skandinavia_pinta',
          'roadmodel_suomi_piste',
          'roadmodel_suomi_airports',
          'roadmodel_nowcast',
          'roadmodel_skyview',
          'roadkriging1km_suomi',
        ],
      },
      {
        parameter: 'kpero',
        producers: ['road'],
      },
    ],
  },
  DriftSnowIndex: {
    name: {
      fi: 'Tuiskuriski',
      en: 'Drift snow index',
    },
    type: 'number',
    min: 0,
    max: 3,
    requestParameters: [{ parameter: 'SNOWDRIFT-N', producers: ['MEPSMTA'] }],
  },
  FeelsLike: {
    name: {
      fi: '"Tuntuu kuin" lämpötila',
      en: '"Feels like" temperature',
    },
    unit: '°C',
    type: 'number',
  },
  FreezingLevelHeight: {
    name: {
      fi: 'Lämpötilan nollarajan korkeus',
      en: 'Freezing temperature height',
    },
    unit: 'm',
    type: 'number',
    requestParameters: [{ parameter: 'H0C-M', producers: ['ECGMTA', 'MEPS'] }],
  },
  Friction: {
    name: {
      fi: 'Kitka',
      en: 'Friction',
    },
    type: 'number',
    requestParameters: [
      {
        parameter: 'kitka',
        producers: ['road'],
      },
    ],
  },
  FrostSum: {
    name: {
      fi: 'Kuurasumma',
      en: 'Frost sum',
    },
    type: 'number',
  },
  RoadFrostProbability: {
    name: {
      fi: 'Tiestön kuuraantumisen todennäköisyys',
      en: 'Probability of road frost formation',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
    requestParameters: [
      {
        parameter: 'FrostProbabilty',
        producers: ['roadmodel_probability_grid', 'roadmodel_probability_grid'],
      },
    ],
  },
  GroundTemperature: {
    name: {
      fi: 'Tien maalämpötila',
      en: 'Road ground temperature',
    },
    unit: '°C',
    type: 'number',
  },
  HighCloudCover: {
    name: {
      fi: 'Yläpilvisyys',
      en: 'High cloud cover',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  HourlyMaximumGust: {
    name: {
      fi: 'Tunnin voimakkain puuska',
      en: 'Hourly maximum gust',
    },
    unit: 'm/s',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'FFG-MS', producers: ['ECG', 'MEPS'] },
      { parameter: 'wg', producers: ['road'] },
    ],
  },
  LowCloudCover: {
    name: {
      fi: 'Alapilvisyys',
      en: 'Low cloud cover',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  MiddleCloudCover: {
    name: {
      fi: 'Keskipilvisyys',
      en: 'Middle cloud cover',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  PoP: {
    name: {
      fi: 'Sateen todennäköisyys',
      en: 'Probability of precipitation',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  PotentialPrecipitationForm: {
    name: {
      fi: 'Potentiaalinen sateen olomuoto',
      en: 'Potential precipitation form',
    },
    type: 'mapped',
    values: [
      { value: 0, name: { en: 'Drizzle', fi: 'Tihkusadetta' } },
      { value: 1, name: { en: 'Rain', fi: 'Vesisadetta' } },
      { value: 2, name: { en: 'Sleet', fi: 'Räntää' } },
      { value: 3, name: { en: 'Snow', fi: 'Lumisadetta' } },
      { value: 4, name: { en: 'Freezing drizzle', fi: 'Jäätävää tihkua' } },
      { value: 5, name: { en: 'Freezing rain', fi: 'Jäätävää sadetta' } },
      { value: 6, name: { en: 'Hail', fi: 'Rakeita' } },
      { value: 7, name: { en: 'Snow grains', fi: 'Lumijyväsiä' } },
      { value: 8, name: { en: 'Ice pellets', fi: 'Jäätä' } },
    ],
    requestParameters: [
      { parameter: 'POTPRECF-N', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  Precipitation1h: {
    name: {
      fi: '1h sadekertymä',
      en: '1h precipitation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'RR-1-MM', producers: ['ECGMTA', 'MEPSMTA'] },
      {
        parameter: 'Precipitation1h',
        producers: [
          'smartmet_nowcast_ppn_scandinavia_surface',
          'road',
        ],
      },
      {
        parameter: 'RadarPrecipitation1h',
        producers: ['radar_finland_hourly_sums'],
      },
    ],
  },
  Precipitation3h: {
    name: {
      fi: '3h sadekertymä',
      en: '3h precipitation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'RR-3-MM', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  Precipitation6h: {
    name: {
      fi: '6h sadekertymä',
      en: '6h precipitation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'RR-6-MM', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  Precipitation12h: {
    name: {
      fi: '12h sadekertymä',
      en: '12h precipitation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
  },
  Precipitation24h: {
    name: {
      fi: '24h sadekertymä',
      en: '24h precipitation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'RR-24-MM', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  PrecipitationForm: {
    name: {
      fi: 'Sateen olomuoto',
      en: 'Precipitation form',
    },
    type: 'mapped',
    values: [
      // GRIB2 values
      { value: 11, name: { en: 'Drizzle', fi: 'Tihkusadetta' } },
      { value: 1, name: { en: 'Rain', fi: 'Vesisadetta' } },
      { value: 7, name: { en: 'Sleet', fi: 'Räntää' } },
      { value: 5, name: { en: 'Snow', fi: 'Lumisadetta' } },
      { value: 12, name: { en: 'Freezing drizzle', fi: 'Jäätävää tihkua' } },
      { value: 3, name: { en: 'Freezing rain', fi: 'Jäätävää sadetta' } },
    ],
    requestParameters: [
      { parameter: 'PRECFORM2-N', producers: ['default', 'ECGMTA', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfRailRule5: {
    name: {
      fi: 'Raideliikenteen 5 säännön todennäköisyys',
      en: 'Rail rule 5 probability',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'RAIL-MEAN-N', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver1mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 1 mm',
      en: 'Daily precipitation probability ≥ 1 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-1', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver10mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 10 mm',
      en: 'Daily precipitation probability ≥ 10 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-2', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver20mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 20 mm',
      en: 'Daily precipitation probability ≥ 20 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-3', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver30mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 30 mm',
      en: 'Daily precipitation probability ≥ 30 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-4', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver50mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 50 mm',
      en: 'Daily precipitation probability ≥ 50 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-5', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver70mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 70 mm',
      en: 'Daily precipitation probability ≥ 70 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-6', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  DailyPrecipitationProbabilityOver100mm: {
    name: {
      fi: 'Vuorokausisateen todennäköisyys ≥ 100 mm',
      en: 'Daily precipitation probability ≥ 100 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RR24-7', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  SnowfallProbability1hOver03mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 0.3 mm',
      en: '1h snowfall probability ≥ 0.3 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-1', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability1hOver1mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 1 mm',
      en: '1h snowfall probability ≥ 1 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-2', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability1hOver2mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 2 mm',
      en: '1h snowfall probability ≥ 2 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-3', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability1hOver4mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 4 mm',
      en: '1h snowfall probability ≥ 4 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-4', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability1hOver6mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 6 mm',
      en: '1h snowfall probability ≥ 6 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-5', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability1hOver10mm: {
    name: {
      fi: '1h lumisateen todennäköisyys ≥ 10 mm',
      en: '1h snowfall probability ≥ 10 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-6', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability3hOver10mm: {
    name: {
      fi: '3h lumisateen todennäköisyys ≥ 10 mm',
      en: '3h snowfall probability ≥ 10 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN3-4', producers: ['MEPSMTA'] }],
  },
  SnowfallProbability6hOver1mm: {
    name: {
      fi: '6h lumisateen todennäköisyys ≥ 1 mm',
      en: '6h snowfall probability ≥ 1 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-1', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability6hOver5mm: {
    name: {
      fi: '6h lumisateen todennäköisyys ≥ 5 mm',
      en: '6h snowfall probability ≥ 5 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-2', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability6hOver10mm: {
    name: {
      fi: '6h lumisateen todennäköisyys ≥ 10 mm',
      en: '6h snowfall probability ≥ 10 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-3', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability6hOver20mm: {
    name: {
      fi: '6h lumisateen todennäköisyys ≥ 20 mm',
      en: '6h snowfall probability ≥ 20 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN-4', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver1mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 1 mm',
      en: '24h snowfall probability ≥ 1 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-1', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver5mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 5 mm',
      en: '24h snowfall probability ≥ 5 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-2', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver10mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 10 mm',
      en: '24h snowfall probability ≥ 10 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-3', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver15mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 15 mm',
      en: '24h snowfall probability ≥ 15 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-4', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver30mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 30 mm',
      en: '24h snowfall probability ≥ 30 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-5', producers: ['ECM_PROB'] }],
  },
  SnowfallProbability24hOver50mm: {
    name: {
      fi: '24h lumisateen todennäköisyys ≥ 50 mm',
      en: '24h snowfall probability ≥ 50 mm',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [{ parameter: 'PROB-SN24-6', producers: ['ECM_PROB'] }],
  },
  Pressure: {
    name: {
      fi: 'Pintapaine',
      en: 'Surface pressure',
    },
    unit: 'hPa',
    type: 'number',
    min: 0,
  },
  ProbabilityDryRoad: {
    name: {
      fi: 'Kuivan kelin todennäköisyys',
      en: 'Probability of dry road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityFrostyRoad: {
    name: {
      fi: 'Kuuraisen kelin todennäköisyys',
      en: 'Probability of frosty road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityIcyRoad: {
    name: {
      fi: 'Jäisen kelin todennäköisyys',
      en: 'Probability of icy road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityMoistRoad: {
    name: {
      fi: 'Kostean kelin todennäköisyys',
      en: 'Probability of moist road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityPartlyIcyRoad: {
    name: {
      fi: 'Osittain jäisen kelin todennäköisyys',
      en: 'Probability of partly icy road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilitySnowyRoad: {
    name: {
      fi: 'Lumisen kelin todennäköisyys',
      en: 'Probability of snowy road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityWetRoad: {
    name: {
      fi: 'Märän kelin todennäköisyys',
      en: 'Probability of wet road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityWetSnowyRoad: {
    name: {
      fi: 'Sohjoisen kelin todennäköisyys',
      en: 'Probability of slushy road conditions',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityThunderstorm: {
    name: {
      fi: 'Ukkosen todennäköisyys',
      en: 'Probability of thunder',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  ProbabilityOfThunderPOT30: {
    name: {
      fi: 'Ukkosen todennäköisyys taso 2',
      en: 'Probability of thunder level 2',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-POT-2', producers: ['MEPSMTA', 'ECM_PROB'] },
    ],
  },
  ProbabilityTRoadLimit1: {
    name: {
      fi: 'Tienpinnan lämpötilan todennäköisyys alle 0 °C',
      en: 'Probability of road temperature below 0 °C',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 1,
  },
  ProbabilityTRoadLimit2: {
    name: {
      fi: 'Tienpinnan lämpötilan todennäköisyys alle -6 °C',
      en: 'Probability of road temperature below -6 °C',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 1,
  },
  ProbabilityOfPrecFormDrizzle: {
    name: {
      fi: 'Tihkun todennäköisyys',
      en: 'Probability of drizzle',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-DRIZZLE', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfPrecFormFreezingDrizzle: {
    name: {
      fi: 'Jäätävän tihkun todennäköisyys',
      en: 'Probability of freezing drizzle',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-FRDRZZL', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfPrecFormRain: {
    name: {
      fi: 'Vesisateen todennäköisyys',
      en: 'Probability of rain',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-RAIN', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfPrecFormFreezingRain: {
    name: {
      fi: 'Jäätävän sateen todennäköisyys',
      en: 'Probability of freezing rain',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-FRRAIN', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfPrecFormSleet: {
    name: {
      fi: 'Räntäsateen todennäköisyys',
      en: 'Probability of sleet',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-SLEET', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfPrecFormSnow: {
    name: {
      fi: 'Lumisateen todennäköisyys',
      en: 'Probability of snow',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-SNOW', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  ProbabilityOfSeaLevelLimit1: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - keltainen (matala)',
      en: 'Sea level probability - yellow (low)',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit2: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - keltainen (korkea)',
      en: 'Sea level probability - keltainen (high)',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit3: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - oranssi',
      en: 'Sea level probability - orange',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit4: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - punainen',
      en: 'Sea level probability - red',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit1N2000: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - keltainen (matala) N2000',
      en: 'Sea level probability - yellow (low) N2000',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit2N2000: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - keltainen (korkea) N2000',
      en: 'Sea level probability - yellow (high) N2000',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit3N2000: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - oranssi N2000',
      en: 'Sea level probability - orange N2000',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  ProbabilityOfSeaLevelLimit4N2000: {
    name: {
      fi: 'Vedenkorkeuden todennäköisyys - punainen N2000',
      en: 'Sea level probability - red N2000',
    },
    type: 'number',
    unit: '%',
    min: 0,
    max: 100,
  },
  QNH: {
    name: {
      fi: 'QNH',
      en: 'QNH',
    },
    unit: 'hPa',
    type: 'number',
    min: 930,
    max: 1080,
    requestParameters: [
      { parameter: 'QNH-HPA', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  RadarPrecipitationAccumulation5min: {
    name: {
      fi: '5 min tutkasadekertymähavainto',
      en: '5 min radar precipitation accumulation',
    },
    unit: 'mm/h',
    type: 'number',
    min: 0,
    requestParameters: [
      {
        parameter: 'Precipitation24h',
        producers: ['radar_finland_daily_sums_5min'],
      },
    ],
  },
  RelativeHumidity: {
    name: {
      fi: 'Suhteellinen kosteus',
      en: 'Relative humidity',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  RoadCondition: {
    name: {
      fi: 'Keli',
      en: 'Road condition',
    },
    type: 'mapped',
    values: [
      { value: 1, name: { en: 'Dry', fi: 'Kuiva' } },
      { value: 2, name: { en: 'Moist', fi: 'Kostea' } },
      { value: 3, name: { en: 'Wet', fi: 'Märkä' } },
      { value: 4, name: { en: 'Wet snow', fi: 'Sohjo' } },
      { value: 5, name: { en: 'Frost', fi: 'Kuurainen' } },
      { value: 6, name: { en: 'Partly icy', fi: 'Osittain jäinen' } },
      { value: 7, name: { en: 'Icy', fi: 'Jäinen' } },
      { value: 8, name: { en: 'Snow', fi: 'Lumi' } },
    ],
    requestParameters: [{ parameter: 'keli', producers: ['road'] }],
  },
  RoadConditionSeverity: {
    name: {
      fi: 'Liikenneindeksi',
      en: 'Road condition severity',
    },
    type: 'mapped',
    values: [
      { value: 1, name: { en: 'Normal', fi: 'Normaali' } },
      { value: 2, name: { en: 'Poor', fi: 'Huono' } },
      { value: 3, name: { en: 'Severe', fi: 'Erittäin huono' } },
    ],
  },
  RoadIceCover: {
    name: {
      fi: 'Jään määrä tiestöllä',
      en: 'Road ice cover',
    },
    type: 'number',
    unit: 'mm',
    requestParameters: [
      {
        parameter: 'LI',
        producers: ['road'],
      },
    ],
  },
  RoadFrostCover: {
    name: {
      fi: 'Kuuran määrä tiestöllä',
      en: 'Road frost cover',
    },
    type: 'number',
    unit: 'mm',
  },
  RoadNotification: {
    name: {
      fi: 'Kelihuomautus',
      en: 'Road notification',
    },
    type: 'mapped',
    values: [
      { value: 1, name: { en: 'Frost', fi: 'Kuuraa' } },
      { value: 2, name: { en: 'Partially ice', fi: 'Osittain jäinen' } },
      { value: 3, name: { en: 'Ice', fi: 'Jäinen' } },
    ],
  },
  RoadSnowCover: {
    name: {
      fi: 'Lumen määrä tiestöllä',
      en: 'Road snow cover',
    },
    type: 'number',
    unit: 'mm',
    requestParameters: [
      {
        parameter: 'LS',
        producers: ['road'],
      },
    ],
  },
  RoadTemperature: {
    name: {
      fi: 'Tienpinnan lämpötila',
      en: 'Road surface temperature',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF0: {
    name: {
      fi: 'Tienpinnan lämpötilan F0',
      en: 'Road temperature F0',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF10: {
    name: {
      fi: 'Tienpinnan lämpötilan F10',
      en: 'Road temperature F10',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF25: {
    name: {
      fi: 'Tienpinnan lämpötilan F25',
      en: 'Road temperature F25',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF50: {
    name: {
      fi: 'Tienpinnan lämpötilan F50',
      en: 'Road temperature F50',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF75: {
    name: {
      fi: 'Tienpinnan lämpötilan F75',
      en: 'Road temperature F75',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF90: {
    name: {
      fi: 'Tienpinnan lämpötilan F90',
      en: 'Road temperature F90',
    },
    unit: '°C',
    type: 'number',
  },
  RoadTemperatureF100: {
    name: {
      fi: 'Tienpinnan lämpötilan F100',
      en: 'Road temperature F100',
    },
    unit: '°C',
    type: 'number',
  },
  RoadWarning: {
    name: {
      fi: 'Vaarallisten keliolosuhteiden luokitus',
      en: 'Severe road conditions',
    },
    type: 'mapped',
    values: [
      { value: 1, name: { en: 'Frost', fi: 'Kuura' } },
      { value: 2, name: { en: 'Ice', fi: 'Jää' } },
      { value: 3, name: { en: 'Snow road cover', fi: 'Luminen tienpinta' } },
      {
        value: 4,
        name: { en: 'Snow and ice road cover', fi: 'Pakkasliukkaus' },
      },
      { value: 5, name: { en: 'Wind warning', fi: 'Tuulivaroitus' } },
      { value: 6, name: { en: 'Blizzard', fi: 'Lumipyry' } },
      { value: 7, name: { en: 'Heavy snow', fi: 'Runsas lumisade' } },
      { value: 8, name: { en: 'Quick change', fi: 'Nopea muutos' } },
      { value: 9, name: { en: 'Sleet on ice', fi: 'Räntää jäiselle' } },
      { value: 10, name: { en: 'Water on ice', fi: 'Vettä jäiselle' } },
      { value: 11, name: { en: 'Alijäähtynyt sade', fi: 'Freezing rain' } },
      { value: 12, name: { en: 'Water slide', fi: 'Vesiliirto' } },
    ],
  },
  SmartSymbol: {
    name: {
      fi: 'Sääsymboli',
      en: 'Weather symbol',
    },
    type: 'mapped',
    values: [
      { value: 1, name: { en: 'Clear', fi: 'Selkeää' } },
      { value: 2, name: { en: 'Mostly clear', fi: 'Enimmäkseen selkeää' } },
      { value: 4, name: { en: 'Partly cloudy', fi: 'Puolipilvistä' } },
      { value: 6, name: { en: 'Mostly cloudy', fi: 'Enimmäkseen pilvistä' } },
      { value: 7, name: { en: 'Overcast', fi: 'Pilvistä' } },
      { value: 9, name: { en: 'Fog', fi: 'Sumua' } },
      {
        value: 71,
        name: { en: 'Isolated thundershowers', fi: 'Yksittäisiä ukkoskuuroja' },
      },
      {
        value: 74,
        name: { en: 'Scattered thundershowers', fi: 'Hajanaisia ukkoskuuroja' },
      },
      { value: 77, name: { en: 'Thundershowers', fi: 'Ukkoskuuroja' } },
      {
        value: 21,
        name: { en: 'Isolated showers', fi: 'Yksittäisiä sadekuuroja' },
      },
      {
        value: 24,
        name: { en: 'Scattered showers', fi: 'Hajanaisia sadekuuroja' },
      },
      { value: 27, name: { en: 'Showers', fi: 'Sadekuuroja' } },
      { value: 11, name: { en: 'Drizzle', fi: 'Tihkusadetta' } },
      { value: 14, name: { en: 'Freezing drizzle', fi: 'Jäätävää tihkua' } },
      { value: 17, name: { en: 'Freezing rain', fi: 'Jäätävää sadetta' } },
      {
        value: 31,
        name: { en: 'Periods of light rain', fi: 'Ajoittain heikkoa sadetta' },
      },
      {
        value: 34,
        name: { en: 'Periods of light rain', fi: 'Ajoittain heikkoa sadetta' },
      },
      { value: 37, name: { en: 'Light rain', fi: 'Heikkoa sadetta' } },
      {
        value: 32,
        name: {
          en: 'Periods of moderate rain',
          fi: 'Ajoittain kohtalaista sadetta',
        },
      },
      {
        value: 35,
        name: {
          en: 'Periods of moderate rain',
          fi: 'Ajoittain kohtalaista sadetta',
        },
      },
      { value: 38, name: { en: 'Moderate rain', fi: 'Kohtalaista sadetta' } },
      {
        value: 33,
        name: {
          en: 'Periods of heavy rain',
          fi: 'Ajoittain voimakasta sadetta',
        },
      },
      {
        value: 36,
        name: {
          en: 'Periods of heavy rain',
          fi: 'Ajoittain voimakasta sadetta',
        },
      },
      { value: 39, name: { en: 'Heavy rain', fi: 'Voimakasta sadetta' } },
      {
        value: 41,
        name: {
          en: 'Isolated light sleet showers',
          fi: 'Yksittäisiä heikkoja räntäkuuroja',
        },
      },
      {
        value: 44,
        name: {
          en: 'Scattered light sleet showers',
          fi: 'Hajanaisia heikkoja räntäkuuroja',
        },
      },
      { value: 47, name: { en: 'Light sleet', fi: 'Heikkoa räntää' } },
      {
        value: 42,
        name: {
          en: 'Isolated moderate sleet showers',
          fi: 'Yksittäisiä kohtalaisia räntäkuuroja',
        },
      },
      {
        value: 45,
        name: {
          en: 'Scattered moderate sleet showers',
          fi: 'Hajanaisia kohtalaisia räntäkuuroja',
        },
      },
      { value: 48, name: { en: 'Moderate sleet', fi: 'Kohtalaista räntää' } },
      {
        value: 43,
        name: {
          en: 'Isolated heavy sleet showers',
          fi: 'Yksittäisiä voimakkaita räntäkuuroja',
        },
      },
      {
        value: 46,
        name: {
          en: 'Scattered heavy sleet showers',
          fi: 'Hajanaisia voimakkaita räntäkuuroja',
        },
      },
      { value: 49, name: { en: 'Heavy sleet', fi: 'Voimakasta räntää' } },
      {
        value: 51,
        name: {
          en: 'Isolated light snow showers',
          fi: 'Yksittäisiä heikkoja lumikuuroja',
        },
      },
      {
        value: 54,
        name: {
          en: 'Scattered light snow showers',
          fi: 'Hajanaisia heikkoja lumikuuroja',
        },
      },
      { value: 57, name: { en: 'Light snowfall', fi: 'Heikkoa lumisadetta' } },
      {
        value: 52,
        name: {
          en: 'Isolated moderate snow showers',
          fi: 'Yksittäisiä kohtalaisia lumikuuroja',
        },
      },
      {
        value: 55,
        name: {
          en: 'Scattered moderate snow showers',
          fi: 'Hajanaisia kohtalaisia lumikuuroja',
        },
      },
      {
        value: 58,
        name: { en: 'Moderate snowfall', fi: 'Kohtalaista lumisadetta' },
      },
      {
        value: 53,
        name: {
          en: 'Isolated heavy snow showers',
          fi: 'Yksittäisiä voimakkaita lumikuuroja',
        },
      },
      {
        value: 56,
        name: {
          en: 'Scattered heavy snow showers',
          fi: 'Hajanaisia voimakkaita lumikuuroja',
        },
      },
      {
        value: 59,
        name: { en: 'Heavy snowfall', fi: 'Voimakasta lumisadetta' },
      },
      {
        value: 61,
        name: { en: 'Isolated hail showers', fi: 'Yksittäisiä raekuuroja' },
      },
      {
        value: 64,
        name: { en: 'Scattered hail showers', fi: 'Hajanaisia raekuuroja' },
      },
      { value: 67, name: { en: 'Hail showers', fi: 'Raekuuroja' } },
    ],
    requestParameters: [
      {
        parameter: 'SmartSymbol-N',
        producers: ['ECGMTA', 'MEPSMTA'],
      },
    ],
  },
  SmartSymbolText: {
    name: {
      fi: 'Vallitseva sää',
      en: 'Current weather',
    },
    type: 'text',
  },
  SnowAccumulation: {
    name: {
      fi: 'Lumikertymä',
      en: 'Snow accumulation',
    },
    unit: 'mm',
    type: 'number',
    min: 0,
    requestParameters: [
      {
        parameter: 'snowaccumulation',
        producers: ['snow_accumulation'],
      },
    ],
  },
  SeaLevel: {
    name: {
      fi: 'Merivedenkorkeus (teoreettisen keskiveden suhteen)',
      en: 'Sea level height (mean water)',
    },
    unit: 'cm',
    type: 'number',
    requestParameters: [
      {
        parameter: 'wlev_pt1s_instant',
        producers: ['observations_fmi'],
      },
    ],
  },
  SeaLevelF10: {
    name: {
      fi: 'Merivedenkorkeus (teoreettisen keskiveden suhteen), min 10',
      en: 'Sea level height (mean water), min 10%',
    },
    unit: 'cm',
    type: 'number',
  },
  SeaLevelF50: {
    name: {
      fi: 'Merivedenkorkeus (teoreettisen keskiveden suhteen), mediaani',
      en: 'Sea level height (mean water), median',
    },
    unit: 'cm',
    type: 'number',
  },
  SeaLevelF90: {
    name: {
      fi: 'Merivedenkorkeus (teoreettisen keskiveden suhteen), max 90',
      en: 'Sea level height (mean water), max 90%',
    },
    unit: 'cm',
    type: 'number',
  },
  SeaLevelN2000: {
    name: {
      fi: 'Merivedenkorkeus (N2000 korkeusjärjestelmässä)',
      en: 'Sea level height (N2000)',
    },
    unit: 'cm',
    type: 'number',
    requestParameters: [
      {
        parameter: 'wlevn2k_pt1s_instant',
        producers: ['observations_fmi'],
      },
    ],
  },
  SeaLevelF10N2000: {
    name: {
      fi: 'Merivedenkorkeus (N2000 korkeusjärjestelmässä), min 10',
      en: 'Sea level height (N2000), min 10%',
    },
    unit: 'cm',
    type: 'number',
  },
  SeaLevelF50N2000: {
    name: {
      fi: 'Merivedenkorkeus (N2000 korkeusjärjestelmässä), mediaani',
      en: 'Sea level height (N2000), median',
    },
    unit: 'cm',
    type: 'number',
  },
  SeaLevelF90N2000: {
    name: {
      fi: 'Merivedenkorkeus (N2000 korkeusjärjestelmässä), max 90',
      en: 'Sea level height (N2000), max 90%',
    },
    unit: 'cm',
    type: 'number',
  },
  TemperatureSea: {
    name: {
      fi: 'Meren pintalämpötila',
      en: 'Sea surface temperature',
    },
    unit: '°C',
    type: 'number',
  },
  FrostProbability: {
    name: {
      fi: 'Hallan todennäköisyys',
      en: 'Frost probability',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 100,
  },
  SevereFrostProbability: {
    name: {
      fi: 'Ankaran hallan todennäköisyys',
      en: 'Severe frost probability',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
  },
  SigWaveHeight: {
    name: {
      fi: 'Merkitsevä aallonkorkeus',
      en: 'Significant wave height',
    },
    unit: 'm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'HWS-M', producers: ['ECGSEA'] },
    ],
  },
  Temperature: {
    name: {
      fi: 'Lämpötila',
      en: 'Temperature',
    },
    unit: '°C',
    type: 'number',
  },
  TemperatureProbabilityLessThan0: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≤ 0 °C',
      en: 'Temperature probability ≤ 0 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TC-0', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityLessThan20: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≤ -20 °C',
      en: 'Temperature probability ≤ -20 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TC-2', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityLessThan25: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≤ -25 °C',
      en: 'Temperature probability ≤ -25 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TC-3', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityLessThan30: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≤ -30 °C',
      en: 'Temperature probability ≤ -30 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TC-4', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityLessThan35: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≤ -35 °C',
      en: 'Temperature probability ≤ -35 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TC-5', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityMoreThan25: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≥ 25 °C',
      en: 'Temperature probability ≥ 25 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TW-1', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityMoreThan27: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≥ 27 °C',
      en: 'Temperature probability ≥ 27 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TW-2', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TemperatureProbabilityMoreThan30: {
    name: {
      fi: 'Lämpötilan todennäköisyys ≥ 30 °C',
      en: 'Temperature probability ≥ 30 °C',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-TW-3', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  TotalCloudCover: {
    name: {
      fi: 'Kokonaispilvisyys',
      en: 'Total cloud cover',
    },
    unit: '%',
    // TODO: Change to mapped and N/8?
    type: 'number',
    min: 0,
    max: 100,
  },
  TotalWireSnowLoadChange: {
    name: {
      fi: 'Kokonaisjohdinkertymän muutos',
      en: 'Total wire load change',
    },
    unit: 'kg/m',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'TotalSnowLoadChange', producers: ['wiresnowload'] },
    ],
  },
  TotalWireSnowLoad: {
    name: {
      fi: 'Kokonaisjohdinkertymä',
      en: 'Total wire snow load',
    },
    unit: 'kg/m',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'TotalSnowLoad', producers: ['wiresnowload'] },
    ],
  },
  TotalCrownSnowLoadChange: {
    name: {
      fi: 'Tykkylumikuorman muutos',
      en: 'Total crown snow load change',
    },
    unit: 'kg/m²',
    type: 'number',
    min: 0,
    requestParameters: [
      {
        parameter: 'TotalSnowLoadChange',
        producers: ['crownsnowload'],
      }
    ]
  },
  TotalCrownSnowLoad: {
    name: {
      fi: 'Tykkylumikuorma',
      en: 'Total crown snow load',
    },
    unit: 'kg/m²',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'TotalSnowLoad', producers: ['crownsnowload'] },
    ],
  },
  Visibility: {
    name: {
      fi: 'Näkyvyys',
      en: 'Visibility',
    },
    unit: 'm',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'VV2-M', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  WaterStorage: {
    name: {
      fi: 'Veden määrä tiestöllä',
      en: 'Road water storage',
    },
    unit: 'mm',
    type: 'number',
    requestParameters: [
      {
        parameter: 'LW',
        producers: ['road'],
      },
    ],
  },
  WindDirection: {
    name: {
      fi: 'Tuulen suunta',
      en: 'Wind direction',
    },
    type: 'number',
    unit: '°',
    min: 0,
    max: 360,
    requestParameters: [
      { parameter: 'DD-D', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  WindGust: {
    name: {
      fi: 'Tuulen puuska',
      en: 'Wind gust',
    },
    unit: 'm/s',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'FFG-MS', producers: ['ECG', 'MEPS'] },
      { parameter: 'wg', producers: ['road'] },
    ],
  },
  WindSpeedMS: {
    name: {
      fi: 'Keskituuli',
      en: 'Mean wind speed',
    },
    unit: 'm/s',
    type: 'number',
    min: 0,
    requestParameters: [
      { parameter: 'FF-MS', producers: ['ECGMTA', 'MEPSMTA'] },
    ],
  },
  MeanWindSpeedProbabilityMoreThan11: {
    name: {
      fi: 'Keskituulen todennäköisyys ≥ 11 m/s',
      en: 'Mean wind speed probability ≥ 11 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-W-1', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  MeanWindSpeedProbabilityMoreThan14: {
    name: {
      fi: 'Keskituulen todennäköisyys ≥ 14 m/s',
      en: 'Mean wind speed probability ≥ 14 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-W-2', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  MeanWindSpeedProbabilityMoreThan17: {
    name: {
      fi: 'Keskituulen todennäköisyys ≥ 17 m/s',
      en: 'Mean wind speed probability ≥ 17 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-W-3', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  MeanWindSpeedProbabilityMoreThan21: {
    name: {
      fi: 'Keskituulen todennäköisyys ≥ 21 m/s',
      en: 'Mean wind speed probability ≥ 21 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-W-4', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  MeanWindSpeedProbabilityMoreThan25: {
    name: {
      fi: 'Keskituulen todennäköisyys ≥ 25 m/s',
      en: 'Mean wind speed probability ≥ 25 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-W-5', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  WindGustProbabilityMoreThan15: {
    name: {
      fi: 'Puuskan todennäköisyys ≥ 15 m/s',
      en: 'Wind gust probability ≥ 15 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-WG-1', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  WindGustProbabilityMoreThan20: {
    name: {
      fi: 'Puuskan todennäköisyys ≥ 20 m/s',
      en: 'Wind gust probability ≥ 20 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-WG-2', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  WindGustProbabilityMoreThan25: {
    name: {
      fi: 'Puuskan todennäköisyys ≥ 25 m/s',
      en: 'Wind gust probability ≥ 25 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-WG-3', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  WindGustProbabilityMoreThan30: {
    name: {
      fi: 'Puuskan todennäköisyys ≥ 30 m/s',
      en: 'Wind gust probability ≥ 30 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-WG-4', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
  WindGustProbabilityMoreThan35: {
    name: {
      fi: 'Puuskan todennäköisyys ≥ 35 m/s',
      en: 'Wind gust probability ≥ 35 m/s',
    },
    unit: '%',
    type: 'number',
    min: 0,
    max: 1,
    requestParameters: [
      { parameter: 'PROB-WG-5', producers: ['ECM_PROB', 'MEPSMTA'] },
    ],
  },
};

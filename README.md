# fmi-weather-metadata

TypeScript definitions for FMI weather parameter metadata.

The data will be released as a public NPM package.

## Content

- Weather parameter names
  - For example observation and forecast data have diffent names/IDs. We would like to use one name/ID for a certain thing
- Translations
- Value intervals for the parameters
- Available values for `enum` type parameters

## License

MIT
